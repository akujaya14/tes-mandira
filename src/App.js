import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import { useEffect, useState } from "react";

var XMLParser = require('react-xml-parser');
const baseURL = "./response.xml";

function App() {
  const [getListHotel, setListHotel] = useState([]);
  
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async() => {
    axios.get(baseURL).then((res) => {
      var xml = new XMLParser().parseFromString(res.data);  
      let data =  xml.children[1].children[0].children[8].children;
      let result = [];
      for (let i = 0; i < data.length; i++) {
        let TripAdvisorRating = 0;
        if(data[i].children[1].children[9] !== undefined){
          TripAdvisorRating = data[i].children[1].children[9].value;
        }
        let x = {
          "HotelCode"         : data[i].children[1].children[0].value,
          "HotelName"         : data[i].children[1].children[1].value,
          "HotelPicture"      : data[i].children[1].children[2].value,
          "HotelDescription"  : data[i].children[1].children[3].value,
          "HotelAddress"      : data[i].children[1].children[6].value,
          "Rating"            : data[i].children[1].children[7].value,
          "TripAdvisorRating" : TripAdvisorRating,
          "Currency"          : data[i].children[2].attributes.Currency,
          "OriginalPrice"     : data[i].children[2].attributes.OriginalPrice,
          "PrefPrice"         : data[i].children[2].attributes.PrefPrice,
          "TotalPrice"        : data[i].children[2].attributes.TotalPrice,
        }
        result.push(x)
      }
      setListHotel(result)
    });

  }

  
  return (
    <div className="App">
      <div className="row filter">
        <div className="col-md-3 filter-item">
          <select className="form-select">
            <option value="">Bintang Hotel</option>
            <option value="1">One</option>
            <option value="2">Two</option>
          </select>
        </div>
      </div>

      <div className="content">
        <div className="container">
          <div className="head-content row align-center">
            <div className="col-md-6 item">
              Menampikan 2.000+ akomodasi terbaik dengan harga terbaik
            </div>
            <div className="col-md-6 row item p-0 align-center">
              <div className='col-md-6  text-end'>
                Tampilan Harga : 
              </div>
              <div className='col-md-6 p-0'>
                <select className="form-select col-md-3">
                  <option value="">Per Kamar Per Malam</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                </select>
              </div>
            </div>
          </div>

          <div className="list-hotel">
            {getListHotel.map((data, i) => (
              <div className="item-hotel row" key={data.HotelCode}>
                <div className="col-md-4 p-0 image">
                  <img src={data.HotelPicture} alt="" />
                </div>
                <div className="desc col-md-8">
                  <div className="title">
                    { data.HotelName }
                  </div>
                  <div className="d-flex mb-2">
                    <div className="rating-star">
                      <span className="star">&#9733;</span>
                      <span className="star">&#9733;</span>
                      <span className="star">&#9733;</span>
                    </div>
                    <div className="address">
                      { data.HotelAddress }
                    </div>
                  </div>
                  <div className="d-flex mb-2">
                    <div className="rating-value">
                      { data.TripAdvisorRating }
                    </div>
                    <div className="rating-value-text">
                      Good
                    </div>
                  </div>
                  <div className="d-flex fasilitas mb-2">
                    <div className="item">
                      <div className="text">
                        <span>&#9829;</span> Indonesian food
                      </div>
                    </div>
                    <div className="item">
                      <div className="text">
                        <span>&#9829;</span> Pembatalan booking
                      </div>
                    </div>
                    <div className="item">
                      <div className="text">
                        <span>&#9829;</span> Sarapan Pagi
                      </div>
                    </div>
                  </div>
                  <div>
                    <img src="icon-asset.png" width="300px" alt="" />
                  </div>
                  <div className="price">
                    <div className="best">
                      Holiday Best Deals
                    </div>
                    <div className="ori-price">
                      { data.Currency } { data.OriginalPrice }
                    </div>
                    <div className="total-price">
                      { data.Currency }  <span>{ data.TotalPrice }</span> 
                    </div>
                    <div className="room">
                      per kamar per malam
                    </div>

                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>

      </div>
    </div>
  );
}

export default App;
